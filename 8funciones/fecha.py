#fecha actual
from datetime import date, datetime

fecha_hoy = date.today()
fecha_hoy_completa = datetime.today()

print(fecha_hoy)
print(fecha_hoy_completa)
#fecha especifica
fecha_especifica = datetime.strptime("25/04/2016 17:30:15", "%d/%m/%Y %H:%M:%S")
print(fecha_especifica)

#fecha actual con formato
fecha_hoy_completa_formato = fecha_hoy_completa.strftime("%d/%m/%y %H:%M:%S")
print(fecha_hoy_completa_formato)

fecha_hoy_completa_formato = fecha_hoy_completa.strftime("DIA: %d MES: %m AÑO: %Y %H:%M:%S")
print(fecha_hoy_completa_formato)

#fecha especifica con formato
fecha_especifica = datetime.strptime("25/04/2016 17:30:15", "%d/%m/%Y %H:%M:%S")
fecha_especifica_formato = fecha_especifica.strftime("DIA: %d MES: %m AÑO: %Y %H:%M:%S")
print(fecha_especifica_formato)