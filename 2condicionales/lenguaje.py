lenguaje = "python"

print (lenguaje, "es")

if lenguaje == "python":
	print ("lenguaje de alto nivel")
elif lenguaje == "c":
    print ("lenguaje clasico")
elif lenguaje == "php":
    print ("lenguaje para la web, lado de backend")
elif lenguaje == "java":
    print ("lenguaje para realizar aplicaciones para android")
elif lenguaje == "javascript":
    print ("lenguaje para la web")
else:
	print ("no es reconocido")

#forma resumida 

if lenguaje == "python" or lenguaje == "c" or lenguaje == "php" or lenguaje == "java":
	print ("lenguaje de programacion")