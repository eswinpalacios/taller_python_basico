class Persona:
	nombres = "Eswin"
	apellidos = "Palacios"
	anio_nacimiento = 1990

	def edad(self):
		return 2016 - self.anio_nacimiento

obj = Persona()
obj.nombres = "Marco"
obj.apellidos = "Sanchez"

print(obj.nombres, "tiene", obj.edad(), "años")